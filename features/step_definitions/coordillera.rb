Given(/^Estoy en el sitio de login$/) do
  visit 'http://www.coordillera.cl'
  
end

When(/^Ingreso un usuario valido$/) do
  fill_in 'login', with: 'mpoblete'
end

When(/^ingreso un password incorrecto$/) do
  fill_in 'password', with: 'Poble.01'
  click_button 'Ingresar'
end

Then(/^Se desplegara un mensaje de error indicando que usuario no fue encontrado$/) do
  page.has_content? ('Error, el usuario no ha sido encontrado')
  sleep 3
end